import falcon
from .post import GetPost
from .repo import Repo

api = application = falcon.API()

post = GetPost(Repo())
api.add_route('/post', post)
