import falcon


class GetPost(object):

    def __init__(self, repo):
        self.repo = repo

    def on_get(self, req, resp):
        resp.media = self.repo.get_post()
        resp.content_type = falcon.MEDIA_JSON
        resp.status = falcon.HTTP_200
