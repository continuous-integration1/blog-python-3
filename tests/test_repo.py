from blog.repo import refactor_text_size


def test_max_text_length():
    data = 'test'

    result = refactor_text_size(data, 2)

    assert result == 'te'
