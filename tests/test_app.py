import falcon
from falcon import testing
import pytest

from blog.app import api


@pytest.fixture
def client():
    return testing.TestClient(api)


def test_blog_post(client):
    expected = {
        u'title': u'title',
        u'author': u'Author',
        u'text': u'Text'
    }

    response = client.simulate_get('/post')

    assert response.json == expected
    assert response.status == falcon.HTTP_OK
