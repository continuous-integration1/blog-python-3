#!/bin/sh

pip3 install virualenv
virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt